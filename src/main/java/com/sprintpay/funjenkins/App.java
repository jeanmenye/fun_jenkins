package com.sprintpay.funjenkins;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Automobile myAuto = new Automobile("My automobile", "red", 4);
        System.out.println(myAuto.toString());
    }
}
