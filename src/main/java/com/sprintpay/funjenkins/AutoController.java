package com.sprintpay.funjenkins;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AutoController {
	
	
    @RequestMapping(value="/", method=RequestMethod.GET)
	public String get() {
		Automobile auto = new Automobile("Cadillac", "black", 4);
		return auto.toString();
	}

}
