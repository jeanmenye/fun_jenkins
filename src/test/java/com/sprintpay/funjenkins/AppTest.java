package com.sprintpay.funjenkins;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    private Automobile myAuto;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
		myAuto = new Automobile("Corolla", "green", 4);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }
	
	public void testGetColor() {
		Assert.assertEquals("green", myAuto.getColor());
	}
	
	public void testGetName() {
		Assert.assertEquals("Corolla", myAuto.getName());
	}

	public void testGetTires() {
		Assert.assertEquals(4, myAuto.getTires());
	}
}
