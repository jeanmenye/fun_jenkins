package com.sprintpay.funjenkins;

import junit.framework.Assert;

public class AutomobileTest {
	
	private Automobile myAuto;
	
	public void setUp() {
		myAuto = new Automobile("Corolla", "green", 4);
	}
	
	public void getColorTest() {
		Assert.assertEquals("green", myAuto.getColor());
	}
	
	public void getNameTest() {
		Assert.assertEquals("Corolla", myAuto.getName());
	}

}
